# Somnium Web Worlds
![](https://i.imgur.com/M1YFUlA.jpg)

## Introduction
Somnium Web Worlds allow you to enhance a Somnium Space parcel with boundless detail, scale, & interactivity for web guests by assigning a custom PlayCanvas game engine scene to it.

A Somnium Web World PlayCanvas scene assigned to a parcel will either augment the regular parcel build, or spawn visitors directly into the standalone scene to provide a holistic custom experience.

## Trailer Video
[![](https://img.youtube.com/vi/juER6v18gfI/0.jpg)](https://www.youtube.com/watch?v=juER6v18gfI)

### Using Web Worlds
It's easy to **use** Web Worlds.  All you need is to own (or rent) a parcel to assign a web world to it.  There is a sample Web Worlds provided by default to try.

Web Worlds are built by Web World Creators. Creators may give and/or trade access to their Web Worlds for parcel owners to use.

Some Web Worlds can be used only on parcels they explicitly designate.  Other Web Worlds allow themselves to be used on any parcel.

### Creating Web Worlds
A Web Worlds SDK is provided to creators.  They use it to build completely custom Web Worlds.  They then give or trade access to their Web Worlds with others - and/or use them on their own parcel(s).

Building a web world is the same as creating a regular PlayCanvas scene. (See Learning PlayCanvas section.) Just about anything is possible.

### Interactivity
By default, Web Worlds are "art-only" & do not load or execute scripts or WASM modules.

However, we are currently testing interactivity.  Whitelisted Web Worlds **are** allowed to load scripts & WASM modules - such as the *RC Vehicles* Web World. (To apply to get your custom Web World whitelisted, join the Web World creators community [in the Discord](https://discord.gg/qwpcVK5EK5).)

## Types of Web Worlds

- Augment Worlds
    - Append themselves to the normal parcel build.
    - Useful for adding extra buildings or event space around a parcel build.
- Holistic Worlds
    - A full scene that visitors spawn into.
    - Useful for providing fully unique experiences to web visitors.
- Hybrid Worlds **(COMING SOON)**
    - Adds a PORTAL to the world onto the normal parcel build.
    - Visitors can explore the normal build, but can also enter the portal to load into the standalone world as well.

### Augment Worlds
![](https://i.imgur.com/mudqyT1.png)

Some types of Web Worlds (Agument Worlds) get added to the normal parcel build to enhance it with interactivity and/or additional physical space.  (An example would be a Web World that is an RC car that guests can drive around any parcel that the Web World is used on.  Or a traveling museum that could attach itself along side any parcel.)

Augment Worlds are great for re-usability across many different parcels - as they can enhance **any** parcel with fun new ways to spend time on them.

### Holistic Worlds
![](https://i.imgur.com/1k22Y2m.png)

For larger more "standalone" experiences, Holistic Worlds spawn guests directly into the parcel's custom Somnium Web Worlds PlayCanvas scene.  In addition to completely unique custom worlds - re-usable worlds also make sense - such as concert venues or meeting halls that could be assigned to a parcel to host a special event.

## How It Works

### How **Using** Web Worlds Works
- Web Worlds are full PlayCanvas scenes that get loaded by Somnium Web.
- You must own (or rent) a parcel in order to assign a Web World to it.
- To assign a web world to your parcel, you log into your parcel in Somnium Web and go to the Parcel Settings menu. In there is a list of whitelisted Web Worlds available for you to assign (or you can paste in a custom URL.)
- Visitors to your parcel in Somnium Web will then be able to see your world either appended to the normal parcel build, or they'll spawn directly into it. (Depending on the world type.)

### How **Creating** Web Worlds Works
- You create & test your scene in the PlayCanvas editor by importing the Web Worlds SDK prefab from the template.
- You can specify which parcel #'s you want your Web World to work on. (Or you can allow it to work on **ANY** parcel that assigns your world's URL.)
- When your world is ready to use, you download a ZIP of it from the PlayCanvas editor & upload it to your own webspace. (See Publishing, Hosting, & Updating section)
- Finally, you assign your Web World's URL to your parcel for visitors to experience.

## Discovering Web Worlds
### Visit Parcels w/ Web Worlds
- **(COMING SOON)** You can find parcels that have Web Worlds on them by clicking the **Explore** button in Somnium Web, then checking the "Filter for parcels w/ worlds" box at the top.
- After joining a parcel with a world on it, you can invite your friends just like you would on a normal parcel.

### Find Web Worlds For **Your** Parcels
Web Worlds are currently in alpha.  To connect with Web World creators, provide feedback on the SDK, & find existing web worlds that you can use on your parcels, please [visit the Somnium Space Discord](https://discord.gg/qwpcVK5EK5).

## Creating Web Worlds
### Learning PlayCanvas
PlayCanvas is a well established game engine.  It has lots of documentation & forkable tutorials. Adapting one of the tutorials is a great place to start experimenting with creating Web Worlds.

Importing art assets into the PlayCanvas editor will be a common task. Models are easiest imported as FBX w/ textures bundled into them - to avoid having to setup their materials manually in the editor.

#### Helpful PlayCanvas Links
- [PlayCanvas Documentation](https://developer.playcanvas.com/)
- [PlayCanvas Forkable Tutorials & Demos](https://developer.playcanvas.com/tutorials/)
- [PlayCanvas User Manual](https://developer.playcanvas.com/user-manual/)
- [PlayCanvas API Reference](https://developer.playcanvas.com/api/)

### Creating & Testing A New Project
1. [Download the Somnium Space Web Worlds Template v0.1.2](https://gitlab.com/somnium-space-public/somnium-space-web-worlds-alpha/-/blob/main/Web_World_Template_v0.1.2.zip)
2. Create a free account at PlayCanvas.com & login.
3. Go to your Projects list on PlayCanvas.com
4. Click Import Project from the side menu.
5. Upload the Web Worlds Template ZIP from Step 1.
7. Open the Main Scene in the new project.
8. Click the **Launch** button to test your project.

The Web Worlds Prefab provides a mock-player controller & parcel platform to test walking around your world inside of the PlayCanvas debugger. These are automatically hidden when your Web World is used on an actual parcel.

For the most part, you just ignore the "Web Worlds Prefab" entity & asset folder. You should not place entities or assets into them directly, to keep things organized.

When you are happy with how your PlayCanvas scene looks, you are ready to move on to publishing & hosting your Web World.

### Publishing, Hosting, & Updating
Getting your Web World ready to be used on a parcel involves the following steps:
1. Open your scene in the PlayCanvas editor.
2. Export a ZIP of your project from the PlayCanvas editor (by clicking Publish.)
3. Unzip & upload its contents to your own web server.
4. Test your project on your web server by going directly to its URL.
    **NOTE:** You must enable CORS for the folder in your web server. ie. ```Access-Control-Allow-Origin "https://somniumspace.com"``` See the Enable CORS section for more help.
6. Go to your parcel in Somnium Web & login.
7. Click on Settings Icon > Parcel Settings.
8. Choose either **Custom Holistic** or **Custom Augment** & paste in your project's URL from step 4.
9. Click **Save & Reload**

**Note:** If you tried to set a web world on your parcel, but now your parcel is failing to load, you can hold SHIFT during the loading screen in order to temporarily disable the web world. From there, you can go back into the Parcel Settings menu & make adjustments.

![](https://i.imgur.com/HI58nbF.png)

### Enable CORS
In order for your web server to allow its files to be loaded across a different domain, you must setup something called Cross Origin Resource Sharing (CORS) on the project folder on your web server.

There are multiple ways to do this - depending on your web server. If your web server supports you uploading a ```.htaccess``` file into the folder to define CORS, then saving the following with the file name ```.htaccess``` and uploading it to your project's folder should enable CORS for you:
```
Header set Access-Control-Allow-Origin "https://somniumspace.com"
```

Remember - if your project works fine on your own domain, but fails to load onto your parcel, an issue with how CORS is setup on your server is likely to blame.

### GitHub Pages Hosting (For people that need a web host.)
If you don't have your own web space, you can setup a free GitHub account that can be used to host your Web World project files.

To go this route, you create a new repository on GitHub, then go to its Settings tab & turn on GitHub Pages for the repo. Then you can use your new GitHub Pages site to host your Web World project files. (Note that you must also create an empty .nojekyll in the root of the repo for this to work.)

### Connecting w/ Builder Community
Whether you are looking for worlds & world builders to use on your own parcels, or looking to learn how to create your own worlds from scratch, the best place to connect with the builder community is by joining [the Somnium Space Discord](https://discord.gg/qwpcVK5EK5).

There are also weekly build streams that take place every week at 9am Pacific on the [Somnium Space Twitch channel](https://www.twitch.tv/somniumspace).

### SDK Update Guide
Updating the SDK version in your old Web Worlds is optional. However, if you want to use features that are only available in the newest SDK version, you'll want to update your project.

#### First, open both projects up:
1. Create a new project using the newest SDK template & open it.
2. Open your old project in another tab so you can switch between them.

#### Next, delete the old Web Worlds Prefab asset folder:
1. **IMPORTANT: Make sure none of your own assets are inside of the "Web Worlds Prefab" folder or any of its sub-folders! We will delete the entire prefab folder in the next step.**
2. Right-click on the ```Web Worlds Prefab``` folder in the ASSETS panel & delete it.

#### Then paste in the new prefab asset folder:
1. Switch to the newest SDK project, copy **its** ```Web Worlds Prefab``` folder in the ASSETS panel & copy it.
2. Switch back to your old project, right-click on the **root** folder in the ASSETS panel (it's labeled as a forward slash at the top) & paste.

#### Next, delete the old Web Worlds Prefab **entity** from the scene hierarchy:
1. **IMPORTANT: Make sure none of your own enities are inside of the "Web Worlds Prefab" entity in the scene hierarchy! We will delete the entire entity from the hierarchy in the next step.**
2. Right-click on the ```Web Worlds Prefab``` entity in the SCENE HIERARCHY & delete it.

#### Then paste in the new Web Worlds Prefab entity:
1. Switch to the newest SDK project, copy **its** ```Web Worlds Prefab``` entity from the SCENE HIERARCHY & copy it.
2. Switch back to your old project, right-click on the **root** of the scene hierarchy (it's usually labeled as "Root") & paste.

#### Finally, click the re-parse button on updated scripts (only needed for UPDATED scripts.)
1. In the ASSET panel, go to into the ```Web Worlds Prefab/Assets``` folder.
2. **Left**-click on each existing script asset that has been updated by the SDK to select them, then click on **PARSE** button on the far-right panel for the selected script.

And that'll do it. You world will be on the newest SDK, and existing entities in your world that used scripts that were updated will all be given valid default values for the new script properties when you re-parsed the scripts.

You can now re-build your Web World & update it on your server.

### SDK Version History
#### v0.1.1 -> v0.1.2 (4/3/2023)
- Added new scripts: ```_som_remoteTextureLoader.js, _som_videoTextureManager.js```
- Added Remote Texture Loader prefab.
- Added Video Texture Manager prefab.
- Added Intro Camera entity to sample scene.
- SDK objects no longer have to be at root level of scene.
- SDK objects that are disabled in the hierarchy are now ignored.
#### v0.1.0 -> v0.1.1 (12/5/2022)
- Updated scripts: ```_som_webWorld.js, _som_webBrowser.js```
- Added Texture Scroll Manager prefab.
- Added more attributes to Web Browser prefab. (Resolution, AutoPlay, UI Text, UI button options, UI thumbnail non-Basis texture image support.)

## Roadmap
Web Worlds are currently in their alpha stage, but you are invited to help shape the evolution of the Web Worlds SDK by participating as world builders even at this early stage.

Below is the roadmap of how the Web Worlds SDK will shape up over its evolution.

### Stage 1
- The initial alpha version.
- Support for Augment & Holistic worlds.
- Provides PlayCanvas template project to fork for testing & development.
- World builders self-host their custom worlds on their own web space.
- "Art-only" worlds by default. (No script execution.)
- No built-in multiplayer syncing of objects.

### Stage 2
- Expanded set of interactive prefabs that "art-only" worlds can use.
- Built-in syncing of interactive prefabs.
- Support for Hybrid worlds.

### Stage 3
- Refactored bridge between Web Worlds & Somnium Web scripting.
- Script-enabled worlds.
- Official script API for accessing user input & manipulating the player.
- Official ledger of Web Worlds for parcel owners to use. (But worlds themselves still self-hosted by their creators.)

### Stage 4
- Support for hosting Web Worlds in the Somnium Space cloud (instead of self-hosted.)
- Further refined & expanded official API for scripts interacting with the user & Somnium Web.
- NFT ownership of Web Worlds.